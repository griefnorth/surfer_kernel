#!/bin/sh
LNX=`pwd`

#find $LNX	\            
#	-path "$LNX/arch/*" ! -path "$LNX/arch/arm*" -prune -o   \    #排除arch目录,而保留arch/arm目录
#	-path "$LNX/tmp*" -prune -o       \                           #排除tmp目录
#	-path "$LNX/Documentation*" -prune -o      \
#	-path "$LNX/scripts*" -prune -o       \
#	-name "*.h" -o \
#	-name "*.c" -o \
#	-name "*.S" -o -print > cscope.files

echo "==find and filter files to cscope.files=="
#find  $LNX \
#  -path "$LNX/arch/m32r" -prune -o \
#  -path "$LNX/drivers/net/wireless/mv8686" -prune -o \
#  -name "*.h" -o \
#  -name "*.c" -o -print > cscope.files

find $LNX -path "$LNX/arch/arm/mach-*" ! -path "$LNX/arch/arm/mach-rk*" -prune -o -name "*.h" -o -name "*.c" > cscope.files

echo "execute cscope..."
cscope -Rbqk -i cscope.files
echo "execute ctags..."
ctags -R
echo "ok!!!"
exit

