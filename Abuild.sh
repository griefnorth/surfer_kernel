#!/bin/sh
export ARCH=arm
export SUBARCH=arm
export CROSS_COMPILE=/home/sanek/build/toolchain/arm-eabi-4.6/bin/arm-eabi-

# file verdion 1.0.0.8

set -e
LNX=`pwd`

# color ouput setting
YELLOW='\033[33m'
RED='\033[31m'
BLUE='\033[34m'
GREEN='\033[32m'
ULINE='\33[4m'

SWRN="warning:"
SERR="error:"
SNOTE="notice:"


P0=$0
P1=$1
P2=$2

PLIST="S12 101S N91 971S N81S N71S N71SI A10"

# commom function define
WRN()
{
	echo "$YELLOW $SWRN $*"
	echo '\033[0m'
}

ERR()
{
	echo "$RED $SERR $*"
	echo '\033[0m'
}

NOTICE()
{
	echo "$GREEN $SNOTE $*"
	echo '\033[0m'
}

delete_projects_display_file()
{
	for item in $PLIST; 
	do
	    rm -f $item.bin
	done;
}


store_defconfig()
{
    NOTICE "store the $P1""_defconfig to configs folder"
    mdefconfigure=$P1"_defconfig"
	if [ -e "$LNX/.config" ]; then
		WRN "$mdefconfigure file saveing..."
		cp .config $LNX/arch/arm/configs/$mdefconfigure
		NOTICE "---->ok!!!"
		exit 0
	else
		ERR "the .config not exist!!!"
		exit 1
	fi
}

load_defconfig()
{
    NOTICE "load the $P1""_defconfig to current system"
    mdefconfigure=$P1"_defconfig"
	if [ -e "$LNX/arch/arm/configs/$mdefconfigure" ]; then
		WRN "loading the $mdefconfigure..."
		# cp $LNX/arch/arm/configs/$mdefconfigure $LNX/.configa
		delete_projects_display_file
		make $mdefconfigure -j4
		echo "$P1" > $P1.bin
		NOTICE "---->ok!!!"
		exit 0
	else
		ERR "the $mdefconfigure not exist!!!"
		exit 1
	fi
}

remove_defconfig()
{
    NOTICE "remove the $P1""_defconfig from current system"
    mdefconfigure=$P1"_defconfig"
	if [ -e "$LNX/arch/arm/configs/$mdefconfigure" ]; then
		WRN "remove the $mdefconfigure..."
		rm $LNX/arch/arm/configs/$mdefconfigure -f
		delete_projects_display_file
		# make $mdefconfigure -j4
		NOTICE "---->ok!!!"
		exit 0
	else
		ERR "the $mdefconfigure not exist!!!"
		exit 1
	fi
}

filter_item_input()
{
    bool_item_in_list=0

    NOTICE ":$PLIST--$ULINE $P1 "
	
	for item in $PLIST; 
	do
		if test $item = $P1; then
			bool_item_in_list=1
		fi
	done;

	if [ $bool_item_in_list -eq 0 ]; then
		WRN "$P1 not exist in current system,now exit!!!"
		exit 0 
	fi
}

compare_with_configs_currentfile()
{
	mdefconfigure=$P1"_defconfig"
	if [ ! -e "$LNX/arch/arm/configs/$mdefconfigure" ]; then
        WRN "$mdefconfigure not exist,can't be compiled!!!"
		exit 1
	fi
	tempfile=$P1".bin"
	if [ ! -e "$LNX/$tempfile" ]; then 
		WRN "$P1 is not the current configure,please load it at first!!"
		exit 1
	fi
}
# Main()
echo ""
NOTICE "execute shell to configure and compile this kernel system"

if [ -n $P2 ] ; then
case "$P2" in
    "-s" ) store_defconfig ;;
    "-l" ) load_defconfig ;;
    "-d" ) remove_defconfig;;
    "-m" ) make menuconfig -j4;exit 0 ;;
    * ) ;;
esac
fi #if [ $2 -z ] ; then



if [ $# -eq 0 ] ; then
    NOTICE "You Want to Compile $RED $ULINE default "
	WRN "no parameter and compile it default !"
	make kernel.img -j4 #> /dev/null
else
    NOTICE "You Want to Compile $RED $ULINE $1 "

	filter_item_input

	compare_with_configs_currentfile
	
    echo -n "$YELLOW Continue?[Yes/No] "
    read keyboard
    inputkey=${keyboard:=Yes}
    echo '\033[0m'
	
	WRN "------------------$ULINE $0 $* Continue? $inputkey \033[0m$YELLOW-------------------"
	echo "\033[0m"

	case "$inputkey" in
		"Yes" ) make kernel.img -j4;cd .. && make -j4 && ./mk*.sh && cd - ;;
		[Yy] ) make kernel.img -j4;cd .. && make -j4 && ./mk*.sh && cd - ;;
		[Nn] ) exit 0 ;;
        * ) exit 0 ;;
	esac

    # echo '\033[0m'
	WRN "------------------$ULINE $0 $* ok!!! \033[0m$YELLOW-------------------"
	echo "\033[0m"


fi


# Main $0 $1 $2 $3 $4


exit 0


