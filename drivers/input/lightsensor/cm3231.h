#ifndef __CM3217_H__
#define __CM3217_H__

#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
#endif
	 

#define CM3231_DRV_NAME "lightsensor"

#define CMD_REG1 0x20 >> 1
#define CMD_REG2 0x22 >> 1
#define CMD_REG3 0x24 >> 1

#define DATA_REG23 0x23 >> 1
#define DATA_REG21 0x21 >> 1

#define GEG2_Defval 0x7F
#define REG3_FD_IT800 0x00 << 4
#define FD_IT_TIMES1 0x4
//#define REG1_FD_IT800 0x00 << 4
#define REG1_FD_IT200 0x3d

#define REG1_FD_RESET 0x11 << 2

#define FD_IT800 0x00  //800ms
#define FD_IT400 0x20  //400ms
#define FD_IT266 0x40  //266ms
#define FD_IT200 0x60  //200ms
#define FD_IT130 0x80 //130ms
#define FD_IT100 0xa0 //100ms
#define FD_IT80  0xc0 //80ms
#define FD_IT66  0xe0 //66ms

#define IT_TIMESH 0x00 << 2   // 1/2T
#define IT_TIMES1 0x01 << 2  // T
#define IT_TIMES2 0x02 << 2 //2T
#define IT_TIMES4 0x03 << 2 //4T
#define WDM_W     0x01 << 1 //word mode
#define SD        0x01 << 0 //

#define K 1.5 


#define LIGHTSENSOR_IOCTL_MAGIC 'l'
#define LIGHTSENSOR_IOCTL_GET_ENABLED _IOR(LIGHTSENSOR_IOCTL_MAGIC, 1, int *)
#define LIGHTSENSOR_IOCTL_ENABLE _IOW(LIGHTSENSOR_IOCTL_MAGIC, 2, int *)

#define LEVELS_NUM 10
struct cm3231_platform_data {
	uint16_t levels[LEVELS_NUM];
};

struct cm3231_info {
	 struct input_dev *ls_input_dev;
	 struct early_suspend early_suspend;
	 struct i2c_client *i2c_client;
	 struct workqueue_struct *lp_wq;
	 struct delayed_work  ls_work;
	 uint16_t als_code;
	 uint16_t *adc_table;
	 int lightsensor_opened;
	 int als_enable;
	
 };

#endif

