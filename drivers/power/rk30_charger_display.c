	 
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/fb.h>
#include <linux/delay.h>
#include <linux/power_supply.h>
#include <linux/workqueue.h>
#include <asm/unaligned.h>
#include <mach/gpio.h>
#include <mach/iomux.h>
#include <mach/board.h>
#include <asm/uaccess.h>
#include<linux/rk30_charger_display.h>


#if 0
#define DBG(x...)	printk(KERN_INFO x)
#else
#define DBG(x...)
#endif

#define POWER_ON_PIN RK30_PIN6_PA2
#define MAX_PRE_CNT 2
#define DET_CNT   5
#define PWR_ON_THRESHD 5       //power on threshd of capacity
unsigned int   pre_cnt = 0;   //for long press counter 
int charge_disp_mode = 0;
int pwr_on_thrsd = 5;          //power on capcity threshold
struct timer_list blctl_timer;   //backlight contol timer
int timer_cunt = 0;           //timer counter 

int charge_discharge = 0;
int DebuG_CNT,DebuG_CNT_A,DebuG_CNT_B,DebuG_CNT_C=0;
static int logo_rotate_180 = FB_ROTATE_UD;

//extern int board_boot_mode(void);
//extern int boot_mode_init(char * s);
/*
#define FB_ROTATE_UR      0
#define FB_ROTATE_CW      1
#define FB_ROTATE_UD      2
#define FB_ROTATE_CCW     3
*/

extern struct fb_info *g_fb0_inf;
extern void fb_show_charge_logo(struct linux_logo *logo);
extern int  rk_fb_show_charge_logo(struct fb_info *info, int rotate,int x,int y);
extern int fb_show_logo(struct fb_info *info, int rotate);
extern void kernel_power_off(void);

struct linux_logo* g_chargerlogo[10]= {
	&logo_charger00_clut224,&logo_charger01_clut224,
	&logo_charger02_clut224,&logo_charger03_clut224,
	&logo_charger04_clut224,&logo_charger05_clut224,
	&logo_charger06_clut224,&logo_charger07_clut224,
	&logo_charger08_clut224,&logo_lowpower_clut224,
};
#if defined(CONFIG_LCD_SC_N71S)||defined(CONFIG_LCD_SC_N71SI)
struct linux_logo* boot_logo = &logo_n71_clut224;
#else
struct linux_logo* boot_logo = &logo_linux_clut224;
#endif

static void fill_back_ground(void)    //fill background with black
{
//	memset(g_fb0_inf->screen_base,0,g_fb0_inf->fix.smem_len);
if(charge_discharge)
{
  memset(g_fb0_inf->screen_base,0,g_fb0_inf->fix.smem_len);
  }

}

static int charger_logo_display(int index)
{
	struct linux_logo *logo = g_chargerlogo[index];
	//DBG("zpp->trace:%s--%d--->index = %d\n",__FUNCTION__,__LINE__,index);
	fb_show_charge_logo(logo);
#if defined(CONFIG_LCD_FLIP_VERTICAL)
	rk_fb_show_charge_logo(g_fb0_inf, logo_rotate_180,(g_fb0_inf->var.xres-LOGO_X)/2,(g_fb0_inf->var.yres-LOGO_Y)/2);
#else	
	rk_fb_show_charge_logo(g_fb0_inf, FB_ROTATE_UR,(g_fb0_inf->var.xres-LOGO_X)/2,(g_fb0_inf->var.yres-LOGO_Y)/2);
#endif
	
	
	return 0;
}
 
static int show_boot_logo(void)
{
	fill_back_ground();
	fb_show_charge_logo(boot_logo);
	//rk_fb_show_charge_logo(g_fb0_inf, 0,0,0);
#if defined(CONFIG_LCD_FLIP_VERTICAL)	
	fb_show_logo(g_fb0_inf,logo_rotate_180);
#else
	fb_show_logo(g_fb0_inf,FB_ROTATE_UR);
#endif
	return 0;
}


static void init_charger_logo(void)
{
	struct linux_logo *logo;
	unsigned long i;
	for (i = 0; i < sizeof(g_chargerlogo)/4; i++)
	{
		logo = g_chargerlogo[i];
		logo->width = ((logo->data[0] << 8) + logo->data[1]);
		logo->height = ((logo->data[2] << 8) + logo->data[3]);
		logo->clutsize = logo->clut[0];
		logo->data += 4;
		logo->clut += 1;
		//DBG("zpp->trace:%s--%d-logo[%d]-logo->width=%d,logo->height=%d,logo->clutsize=%d\n",__FUNCTION__,__LINE__,i,logo->width,
		//logo->height,logo->clutsize);
	}
}

 
static void blctl_timer_func(unsigned long data)
{
	timer_cunt ++ ;
	if(timer_cunt == 20)
	{
		fill_back_ground();
		rk30_backlight_ctl(0);
        rk30_backlight_set(0);
		timer_cunt = 0;
	}
	else
	 	mod_timer(&blctl_timer, jiffies + msecs_to_jiffies(500));
}
static int play_on_long_press(void)
{
	if(gpio_get_value(POWER_ON_PIN)==0)  //play on presss down
	{
		pre_cnt ++;	
	}
	else                                  //play on release
	{
		pre_cnt = 0;
	}
	
	if(pre_cnt >= MAX_PRE_CNT)          //long press 
	{
		DBG("%s>>>>>>>%ud\n",__func__,pre_cnt);
		pre_cnt = 0;
		return 1;
	}
	else
	{
		return 0;
	}
}

int get_charger_logo_start_num(int rsoc)
{
	int rlogonum = 0;
	//DBG("zpp->trace:%s--%d--->rsoc = %d\n",__FUNCTION__,__LINE__,rsoc);

	/*check charger capacity*/
	
	if(rsoc < 5) {
		rlogonum = 0;
	}
	else if(rsoc < 10) {
		rlogonum = 1;
	}
	else if(rsoc < 20) {
		rlogonum = 2;
	}
	else if(rsoc < 40) {
		rlogonum = 3;
	}
	else if(rsoc < 60) {
		rlogonum = 4;
	}
	else if(rsoc < 70) {
		rlogonum = 5;
	}	
	else if(rsoc < 80) {
		rlogonum = 6;
	}		
	else if(rsoc < 95)
	{
		rlogonum = 7;
	}
	else if(rsoc <= 100)
	{
		rlogonum = 8;
	}
		
	return rlogonum;
}


static int __init pwr_on_thrsd_setup(char *str)
{

	pwr_on_thrsd = simple_strtol(str,NULL,10);
	return 0;
}

__setup("pwr_on_thrsd=", pwr_on_thrsd_setup);


LIST_HEAD(rk_psy_head);  //add by yxj for charge logo display  boot_command_line
//int charger_mode=0;	     	//1:charge,0:not charge
static void add_bootmode_charger_to_cmdline(void)
{
	char *pmode=" androidboot.mode=charger";
	//int off = strlen(saved_command_line);
	char *new_command_line = kzalloc(strlen(saved_command_line) + strlen(pmode) + 1, GFP_KERNEL);
	sprintf(new_command_line, "%s%s", saved_command_line, pmode);
	saved_command_line = new_command_line;
	//strcpy(saved_command_line+off,pmode);

	//int off = strlen(boot_command_line);
	//strcpy(boot_command_line+off,pmode);

	DBG("Kernel command line: %s\n", saved_command_line);
}

//display charger logo in kernel CAPACITY


static int  __init start_charge_logo_display(void)
{
	union power_supply_propval val_status = {POWER_SUPPLY_STATUS_DISCHARGING};
	union power_supply_propval val_capacity ={ 100} ;
	struct power_supply *psy;
	
	int my_rsoc;
	int i = 0;
	int charge_logo_run = 1;	

	mdelay(2000);

	DBG("start_charge_logo_display--board_boot_mode()==%d\n",board_boot_mode());

	if(board_boot_mode() == BOOT_MODE_RECOVERY)  //recovery mode
	{
		DBG("recovery mode \n");
		return 0;

	}

	list_for_each_entry(psy, &rk_psy_head, rk_psy_node)
	{
		psy->get_property(psy,POWER_SUPPLY_PROP_ONLINE,&val_status);
		psy->get_property(psy,POWER_SUPPLY_PROP_CAPACITY,&val_capacity); 
	}

	if((val_capacity.intval < pwr_on_thrsd )&&(!charge_discharge))
	{
		DBG("low power\n");
		init_charger_logo();
		fill_back_ground();
		
		charger_logo_display(9);
		
		for(i=0;i<=25;i++)
		{
			rk30_backlight_ctl(125+i);
			msleep(100); 
		} 	
		
		for(i=0;i<=25;i++)
		{	
			rk30_backlight_ctl(150-i);	
			msleep(10);
		}	
		
		if(!charge_discharge)
		{
			kernel_power_off();
			while(1);
			return 0;
		}
	}

	//low power and charging
	if(charge_discharge)
	{
		list_for_each_entry(psy, &rk_psy_head, rk_psy_node)
		{
			psy->get_property(psy,POWER_SUPPLY_PROP_CAPACITY,&val_capacity); 
			psy->get_property(psy,POWER_SUPPLY_PROP_ONLINE,&val_status);
			if(charge_discharge == 1) val_status.intval = POWER_SUPPLY_STATUS_CHARGING;
		}

		DBG("zpp-->Traced-%s-%d val_status.intval=%d,val_capacity.intval=%d\n",__FUNCTION__,__LINE__,
				val_status.intval,val_capacity.intval);
		
		if(val_status.intval == POWER_SUPPLY_STATUS_CHARGING) 
		{
			charge_disp_mode = 1;
            init_charger_logo();
            fill_back_ground();
			rk30_backlight_ctl(150);
            setup_timer(&blctl_timer, blctl_timer_func,0);
            mod_timer(&blctl_timer, jiffies + msecs_to_jiffies(500));
            DBG("zpp-->Traced-%s-%d charge_logo_run=%d,charge_discharge=%d\n",__FUNCTION__,__LINE__,charge_logo_run,charge_discharge);
                    
			while(charge_logo_run)
        	{
        		list_for_each_entry(psy, &rk_psy_head, rk_psy_node)
				{
			    	psy->get_property(psy,POWER_SUPPLY_PROP_CAPACITY,&val_capacity);
			    	psy->get_property(psy,POWER_SUPPLY_PROP_ONLINE,&val_status);
			    }
				my_rsoc = val_capacity.intval;
				DBG("zpp->trace:%s--%d--val_capacity.intval=%d,charge_discharge=%d\n",__FUNCTION__,__LINE__,val_capacity.intval,charge_discharge);
				msleep(500);
				DBG("zpp->trace:%s--%d\n",__FUNCTION__,__LINE__);
				if(!charge_discharge)
	            {
	            	charger_logo_display(get_charger_logo_start_num(my_rsoc));
	            	/*if(my_rsoc > pwr_on_thrsd)
	            	{
						charge_logo_run = 0;
					}
					else*/
					{
						kernel_power_off();
						while(1);
						return 0;
					}
					//msleep(500);  
					//rk29_backlight_ctl(100);	
	            }
	            else
             	for(i=0;i<=get_charger_logo_start_num(my_rsoc);i++)                
                {
					if(my_rsoc == 100)
                    {
                     	charger_logo_display(get_charger_logo_start_num(my_rsoc));
                    }
                    else
                    {
                    	charger_logo_display(i);
                    }
                    msleep(300);
                    //DBG("zpp->trace:%s--%d-->i=%d\n",__FUNCTION__,__LINE__,i);
                   /* if((val_status.intval != POWER_SUPPLY_STATUS_CHARGING)&&(val_status.intval != POWER_SUPPLY_STATUS_FULL))  
                    {
                     	DBG("power adapter offline,power down>>>>>>>\n");
                 	    del_timer(&blctl_timer);
                     	charge_disp_mode = 0;
						kernel_power_off();
                    }*/
                     DBG("zpp-->Traced-%s-%d charge_logo_run=%d\n",__FUNCTION__,__LINE__,charge_logo_run);
                     if(play_on_long_press())
                     {
                 		DBG("system start>>>>>>>>>>>>\n");
                 		del_timer(&blctl_timer);
                 		rk30_backlight_ctl(150);
                  		charge_logo_run = 0;
                  		charge_disp_mode = 0;                                    
               		 }
				}
			}					
		}
		DBG("charging ... val_capacity=%d\n",val_capacity);
	}
	show_boot_logo();

	/*
	DBG("start_charge_logo_display val_status.intval=%d board_boot_mode()=%d\n",board_boot_mode());
	if(val_status.intval == POWER_SUPPLY_STATUS_CHARGING)
	{
		if(board_boot_mode() != BOOT_MODE_REBOOT)   //do not enter power on charge mode when soft reset
	    {			
			add_bootmode_charger_to_cmdline();
			//boot_mode_init("charge");
			DBG("power in charge mode\n");
		}
	}
	*/

	return 0;
} 

//subsys_initcall_sync(start_charge_logo_display);
fs_initcall_sync(start_charge_logo_display);

