/* drivers/video/backlight/rk30_backlight.c
 *
 * Copyright (C) 2009-2011 Rockchip Corporation.
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/err.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/backlight.h>
#include <linux/fb.h>
#include <linux/clk.h>

#include <linux/earlysuspend.h>
#include <asm/io.h>
#include <mach/board.h>
#include <mach/gpio.h>
#include <mach/iomux.h>

#include "rk30_backlight.h"

/*
 * Debug
 */
#if 0
#define DBG(x...)	printk(KERN_INFO x)
#else
#define DBG(x...)
#endif

#define write_pwm_reg(id, addr, val)        __raw_writel(val, addr+(RK30_PWM01_BASE+(id>>1)*0x20000)+id*0x10)
#define read_pwm_reg(id, addr)              __raw_readl(addr+(RK30_PWM01_BASE+(id>>1)*0x20000+id*0x10))

#if defined(CONFIG_LCD_SC_N71S) || defined(CONFIG_LCD_SC_N71SI)
#define STEP_IN_STEP_OUT 100
#else
#define STEP_IN_STEP_OUT 300
#endif


static struct clk *pwm_clk;
static struct backlight_device *rk30_bl;
static int suspend_flag = 0;

static int rk30_bl_update_status(struct backlight_device *bl)
{
	u32 divh,div_total;
	struct rk30_bl_info *rk30_bl_info = bl_get_data(bl);
	u32 id = rk30_bl_info->pwm_id;
	u32 ref = rk30_bl_info->bl_ref;

	if (suspend_flag)
	{
		return 0;
	}

	if (bl->props.brightness < rk30_bl_info->min_brightness)	/*avoid can't view screen when close backlight*/
		bl->props.brightness = rk30_bl_info->min_brightness;

#if defined(CONFIG_LCD_SC_N71SI)||defined(CONFIG_LCD_SC_N71S)
	div_total = 31000;//read_pwm_reg(id, PWM_REG_LRC);
#else
    div_total = read_pwm_reg(id, PWM_REG_LRC);
#endif	
	if (ref) {
		divh = div_total*(bl->props.brightness)/BL_STEP;
	} else {
		divh = div_total*(BL_STEP-bl->props.brightness)/BL_STEP;
	}
	write_pwm_reg(id, PWM_REG_HRC, divh);

	//DBG(">>>%s-->%d brightness = %d, div_total = %d, divh = %d\n",__FUNCTION__,__LINE__,bl->props.brightness, div_total, divh);
	return 0;
}

static int rk30_bl_update_status_in_boot_charge(struct backlight_device *bl)
{
	u32 divh,div_total;
	struct rk30_bl_info *rk30_bl_info = bl_get_data(bl);
	u32 id = rk30_bl_info->pwm_id;
	u32 ref = rk30_bl_info->bl_ref;
#if defined(CONFIG_LCD_SC_N71SI)||defined(CONFIG_LCD_SC_N71S)
	div_total = 31000;//read_pwm_reg(id, PWM_REG_LRC);
#else
    div_total = read_pwm_reg(id, PWM_REG_LRC);
#endif	
	if (ref) {
		divh = div_total*(bl->props.brightness)/BL_STEP;
	} else {
		divh = div_total*(BL_STEP-bl->props.brightness)/BL_STEP;
	}
	write_pwm_reg(id, PWM_REG_HRC, divh);

	//DBG(">>>%s-->%d brightness = %d, div_total = %d, divh = %d\n",__FUNCTION__,__LINE__,bl->props.brightness, div_total, divh);
	return 0;
}

static int rk30_bl_get_brightness(struct backlight_device *bl)
{
	u32 divh,div_total;
	struct rk30_bl_info *rk30_bl_info = bl_get_data(bl);
	u32 id = rk30_bl_info->pwm_id;
	u32 ref = rk30_bl_info->bl_ref;

	div_total = read_pwm_reg(id, PWM_REG_LRC);
	divh = read_pwm_reg(id, PWM_REG_HRC);

	if (!div_total)
		return 0;

	if (ref) {
		return BL_STEP*divh/div_total;
	} else {
		return BL_STEP-(BL_STEP*divh/div_total);
	}
}

void rk30_backlight_ctl(int brightness)
{
	if(rk30_bl)
	{
		rk30_bl->props.brightness = brightness;
		//rk30_bl_update_status(rk30_bl);	
		if(brightness > 0)
		{
			wise_charge_on();
			//rk30_lcd_disp_power_on();
			backlight_power_on();
			rk30_bl_update_status_in_boot_charge(rk30_bl);			
		}
		else
		{
			rk30_bl_update_status_in_boot_charge(rk30_bl);	
			wise_charge_off();
			//rk30_lcd_disp_power_off();
			backlight_power_off();
		}
	}		
}
EXPORT_SYMBOL(rk30_backlight_ctl);

static struct backlight_ops rk30_bl_ops = {
	.update_status	= rk30_bl_update_status,
	.get_brightness	= rk30_bl_get_brightness,
};

static void rk30_backlight_work_func(struct work_struct *work)
{
	struct rk30_bl_info *rk30_bl_info = bl_get_data(rk30_bl);
	int brightness = 0;
	int i =0;
	suspend_flag = 0;
	backlight_power_on();

	rk30_bl->props.brightness = (rk30_bl->props.brightness < rk30_bl_info->min_brightness ? rk30_bl_info->min_brightness : rk30_bl->props.brightness);
	brightness = rk30_bl->props.brightness;
    for(i =0;i<=brightness ;i++)
    {
        udelay(STEP_IN_STEP_OUT);	
        rk30_bl->props.brightness = i;
        rk30_bl_update_status_in_boot_charge(rk30_bl);		
    }
    
	//rk30_bl_update_status(rk30_bl);
}
static void rk30_backlight_boot_step_in(void)
{
	struct rk30_bl_info *rk30_bl_info = bl_get_data(rk30_bl);
	int brightness = 0;
	int i =0;

	backlight_power_init();

	rk30_bl->props.brightness = (rk30_bl->props.brightness < rk30_bl_info->min_brightness ? rk30_bl_info->min_brightness : rk30_bl->props.brightness);
	brightness = rk30_bl->props.brightness;
    for(i =0;i<=brightness ;i++)
    {
        udelay(STEP_IN_STEP_OUT);
        rk30_bl->props.brightness = i;
        rk30_bl_update_status_in_boot_charge(rk30_bl);		
    }
    

}


static DECLARE_DELAYED_WORK(rk30_backlight_work, rk30_backlight_work_func);

#ifdef CONFIG_HAS_EARLYSUSPEND
static void rk30_bl_suspend(struct early_suspend *h)
{
	struct rk30_bl_info *rk30_bl_info = bl_get_data(rk30_bl);
	int brightness,i = 0;
	
	cancel_delayed_work_sync(&rk30_backlight_work);
	
	rk30_bl->props.brightness = (rk30_bl->props.brightness < rk30_bl_info->min_brightness ? rk30_bl_info->min_brightness : rk30_bl->props.brightness);
	brightness = rk30_bl->props.brightness;
	i =brightness;
	for(i = brightness;i>=0 ;i--)
	{
		rk30_bl->props.brightness = i;
		rk30_bl_update_status_in_boot_charge(rk30_bl);	
		udelay(STEP_IN_STEP_OUT);		
	}
	
	//rk30_bl->props.brightness = 0;
	//rk30_bl_update_status(rk30_bl);
	rk30_bl->props.brightness = brightness;

	DBG(">>>%s-->%d brightness = %d,suspend_flag=%d\n",__FUNCTION__,__LINE__,rk30_bl->props.brightness,suspend_flag);

	backlight_power_off();
	
	if (!suspend_flag) {
		clk_disable(pwm_clk);
		if (rk30_bl_info->pwm_suspend)
			rk30_bl_info->pwm_suspend();
	}

	suspend_flag = 1;
}

static void rk30_bl_resume(struct early_suspend *h)
{
	struct rk30_bl_info *rk30_bl_info = bl_get_data(rk30_bl);
	DBG("%s : %s\n", __FILE__, __FUNCTION__);

	if (rk30_bl_info->pwm_resume)
		rk30_bl_info->pwm_resume();

	clk_enable(pwm_clk);

	schedule_delayed_work(&rk30_backlight_work, msecs_to_jiffies(rk30_bl_info->delay_ms));
	//schedule_work(&rk30_backlight_work);

}

static struct early_suspend bl_early_suspend = {
	.suspend = rk30_bl_suspend,
	.resume = rk30_bl_resume,
	.level = EARLY_SUSPEND_LEVEL_BLANK_SCREEN - 1,
};

int zpp_press_only_once = 1; //default backlight turn on
void rk30_backlight_set(bool on)
{
//	printk("%s: set %d\n", __func__, on);
	if(on)
	{
		zpp_press_only_once = 1;
		//rk30_bl_resume(NULL);
	}
	else
	{
		//rk30_bl_suspend(NULL);
		zpp_press_only_once = 0;
	}
	return;
}
EXPORT_SYMBOL(rk30_backlight_set);
#endif



static int rk30_backlight_probe(struct platform_device *pdev)
{		
	int ret = 0;
	struct rk30_bl_info *rk30_bl_info = pdev->dev.platform_data;
	u32 id  =  rk30_bl_info->pwm_id;
	u32 divh, div_total;
	unsigned long pwm_clk_rate;
	struct backlight_properties props;

	if (rk30_bl) {
		printk(KERN_CRIT "%s: backlight device register has existed \n",__func__);
		return -EEXIST;		
	}

	if (!rk30_bl_info->delay_ms)
		rk30_bl_info->delay_ms = 300;

	if (rk30_bl_info->min_brightness < 0 || rk30_bl_info->min_brightness > BL_STEP)
		rk30_bl_info->min_brightness = BACKLIGHT_SEE_MINVALUE;

	if (rk30_bl_info && rk30_bl_info->io_init) {
		rk30_bl_info->io_init();
	}

	memset(&props, 0, sizeof(struct backlight_properties));
	props.type = BACKLIGHT_RAW;
	props.max_brightness = BL_STEP;
	rk30_bl = backlight_device_register("rk30_bl", &pdev->dev, rk30_bl_info, &rk30_bl_ops, &props);
	if (!rk30_bl) {
		printk(KERN_CRIT "%s: backlight device register error\n",
				__func__);
		return -ENODEV;		
	}

	if (id == 0 || id == 1)
		pwm_clk = clk_get(NULL, "pwm01");
	else if (id == 2 || id == 3)
		pwm_clk = clk_get(NULL, "pwm23");
		
	if (IS_ERR(pwm_clk)) {
		printk(KERN_ERR "failed to get pwm clock source\n");
		return -ENODEV;
	}
	pwm_clk_rate = clk_get_rate(pwm_clk);
	div_total = pwm_clk_rate / PWM_APB_PRE_DIV;

	div_total >>= (1 + (PWM_DIV >> 9));
	div_total = (div_total) ? div_total : 1;

	if(rk30_bl_info->bl_ref) {
		divh = 0;
	} else {
		divh = div_total;
	}

	clk_enable(pwm_clk);
	write_pwm_reg(id, PWM_REG_CTRL, PWM_DIV|PWM_RESET);
	write_pwm_reg(id, PWM_REG_LRC, div_total);
	write_pwm_reg(id, PWM_REG_HRC, divh);
	write_pwm_reg(id, PWM_REG_CNTR, 0x0);
	write_pwm_reg(id, PWM_REG_CTRL, PWM_DIV|PWM_ENABLE|PWM_TIME_EN);

	rk30_bl->props.power = FB_BLANK_UNBLANK;
	rk30_bl->props.fb_blank = FB_BLANK_UNBLANK;
	rk30_bl->props.brightness = BL_STEP / 2;

	//schedule_delayed_work(&rk30_backlight_work, msecs_to_jiffies(rk30_bl_info->delay_ms));
	//backlight_power_init();
	//schedule_work(&rk30_backlight_work);
	rk30_backlight_boot_step_in();

	register_early_suspend(&bl_early_suspend);

	printk("rk30 Backlight Driver Initialized.\n");
	return ret;
}

static int rk30_backlight_remove(struct platform_device *pdev)
{		
	struct rk30_bl_info *rk30_bl_info = pdev->dev.platform_data;

	if (rk30_bl) {
		backlight_device_unregister(rk30_bl);
		unregister_early_suspend(&bl_early_suspend);
		clk_disable(pwm_clk);
		clk_put(pwm_clk);
		if (rk30_bl_info && rk30_bl_info->io_deinit) {
			rk30_bl_info->io_deinit();
		}
		return 0;
	} else {
		DBG(KERN_CRIT "%s: no backlight device has registered\n",
				__func__);
		return -ENODEV;
	}
}

static void rk30_backlight_shutdown(struct platform_device *pdev)
{
	struct rk30_bl_info *rk30_bl_info = pdev->dev.platform_data;

	rk30_bl->props.brightness = 0;
	rk30_bl_update_status(rk30_bl);

	if (rk30_bl_info && rk30_bl_info->io_deinit)
		rk30_bl_info->io_deinit();
}

static struct platform_driver rk30_backlight_driver = {
	.probe	= rk30_backlight_probe,
	.remove = rk30_backlight_remove,
	.driver	= {
		.name	="rk30_backlight",//"rk29_backlight"
		.owner	= THIS_MODULE,
	},
	.shutdown	= rk30_backlight_shutdown,
};

static int __init rk30_backlight_init(void)
{
	platform_driver_register(&rk30_backlight_driver);
	return 0;
}
fs_initcall_sync(rk30_backlight_init);
//arch_initcall_sync(rk30_backlight_init);
