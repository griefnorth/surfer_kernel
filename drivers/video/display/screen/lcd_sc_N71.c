#include <linux/fb.h>
#include <linux/delay.h>
#include "../../rk29_fb.h"
#include <mach/gpio.h>
#include <mach/iomux.h>
#include <mach/board.h>
#include "screen.h"

#ifdef CONFIG_HAS_EARLYSUSPEND
#include <linux/earlysuspend.h>
#endif


#if defined(CONFIG_LCD_SC_N71SI)
/* Base */
#define OUT_TYPE		SCREEN_RGB
#define OUT_FACE		OUT_D888_P666//OUT_P888
#define OUT_CLK			51000000		//45000000
#define LCDC_ACLK       500000000		//500000000		//312000000           //29 lcdc axi DMA Ƶ��

/* Timing */
#define H_PW			30			//��ɨ������
#define H_BP			60
#define H_VD			1024		//1024
#define H_FP			70			//100

#define V_PW			4			//��ɨ������
#define V_BP			19
#define V_VD			600			//600
#define V_FP			15			//18

#define LCD_WIDTH          196
#define LCD_HEIGHT         147

#else

#if 1
/* Base */
#define OUT_TYPE	    SCREEN_RGB
#define OUT_FACE	    OUT_D888_P666
#define OUT_CLK	          55000000 //40~51~67
#define LCDC_ACLK        312000000//300000000           //29 lcdc axi DMA Ƶ��

/* Timing */
#define H_PW			32
#define H_BP			320
#define H_VD			1024
#define H_FP			48

#define V_PW			3
#define V_BP			35
#define V_VD			600
#define V_FP			2

#define LCD_WIDTH          196
#define LCD_HEIGHT         147

//#else    //����΢
//
///* Base */
//#define OUT_TYPE	    SCREEN_RGB
//#define OUT_FACE	    OUT_D888_P666
//#define OUT_CLK	          33000000 
//#define LCDC_ACLK        312000000//300000000           //29 lcdc axi DMA Ƶ��
//
///* Timing */
//#define H_PW			30
//#define H_BP			16
//#define H_VD			800
//#define H_FP			210
//
//#define V_PW			13
//#define V_BP			10
//#define V_VD			480
//#define V_FP			22
//
//#define LCD_WIDTH       154    //need modify
//#define LCD_HEIGHT      85

#else    //��Դ

/* Base */
#define OUT_TYPE	    SCREEN_RGB
#define OUT_FACE	    OUT_D888_P666
#define OUT_CLK	          44900000 
#define LCDC_ACLK        312000000//300000000           //29 lcdc axi DMA Ƶ��

/* Timing */
#define H_PW			1
#define H_BP			160
#define H_VD			1024
#define H_FP			160

#define V_PW			1
#define V_BP			23
#define V_VD			600
#define V_FP			12

#define LCD_WIDTH          196
#define LCD_HEIGHT         147

#endif

#endif

/* Other */
#define DCLK_POL		0
#define SWAP_RB		0

#if defined(CONFIG_LCD_SC_N71SI)
int init(void);
int standby(u8 enable);

static struct rk29lcd_info *gLcd_info = NULL;

#define TXD_PORT        RK30_PIN4_PD2
#define CLK_PORT        RK30_PIN4_PD1
#define CS_PORT         RK30_PIN4_PD4

#define CS_OUT()        gpio_direction_output(CS_PORT, 1)
#define CS_SET()        gpio_direction_output(CS_PORT, 1)//gpio_set_value(CS_PORT, GPIO_HIGH)
#define CS_CLR()        gpio_direction_output(CS_PORT, 0)//gpio_set_value(CS_PORT, GPIO_LOW)

#define CLK_OUT()       gpio_direction_output(CLK_PORT, 1)
#define CLK_SET()       gpio_direction_output(CLK_PORT, 1)//gpio_set_value(CLK_PORT, GPIO_HIGH)
#define CLK_CLR()       gpio_direction_output(CLK_PORT, 0)//gpio_set_value(CLK_PORT, GPIO_LOW)

#define TXD_OUT()       gpio_direction_output(TXD_PORT, 1)
#define TXD_SET()       gpio_direction_output(TXD_PORT, 1)//gpio_set_value(TXD_PORT, GPIO_HIGH)
#define TXD_CLR()       gpio_direction_output(TXD_PORT, 0)//gpio_set_value(TXD_PORT, GPIO_LOW)

void init_lcd_io( void )
{
	rk30_mux_api_set(GPIO4D4_SMCDATA12_TRACEDATA12_NAME, GPIO4D_GPIO4D4);
	gpio_request(CS_PORT,NULL);
	CS_OUT();
	rk30_mux_api_set(GPIO4D1_SMCDATA9_TRACEDATA9_NAME, GPIO4D_GPIO4D1);
	gpio_request(CLK_PORT,NULL);	
	CLK_OUT();
	rk30_mux_api_set(GPIO4D2_SMCDATA10_TRACEDATA10_NAME, GPIO4D_GPIO4D2);
	gpio_request(CLK_PORT,NULL);
	TXD_OUT();
}

void resume_lcd_io( void )
{
	init_lcd_io();
}

void suspend_lcd_io( void )
{
	TXD_CLR() ;
	CLK_CLR();
	CS_CLR();
	//mdelay(50);
	//rk30_mux_api_set(GPIO4D4_SMCDATA12_TRACEDATA12_NAME, GPIO4D_SMC_DATA12);
	//rk30_mux_api_set(GPIO4D1_SMCDATA9_TRACEDATA9_NAME, GPIO4D_SMC_DATA9);
	//rk30_mux_api_set(GPIO4D2_SMCDATA10_TRACEDATA10_NAME, GPIO4D_SMC_DATA10);
}

void WriteCommand( int  Command)
{
  unsigned char i,count1, count2;
  count1= Command>>8;
  count2= Command;

  count1= (count1<<2)&0xfc;
  
  CS_CLR();
  //high 8 bit
  for (i=0;i<8;i++)
  {
    //CLK_SET();
    //udelay(20);
    CLK_CLR();
    if (count1 & 0x80) 
    {
    	TXD_SET();
    }
    else
    {
    	TXD_CLR();
    }
 
    //CLK_CLR();
    udelay(20);
    CLK_SET();
    udelay(20);           
    count1<<=1;
  }
  //low 8 bit
  for (i=0;i<8;i++)
  {
    //CLK_SET();
    //udelay(20);    
    CLK_CLR();
    if (count2 & 0x80) 
    {
    	TXD_SET();
    }
    else
    {
    	TXD_CLR();
    } 
    //CLK_CLR();
    udelay(20);
    CLK_SET();
    udelay(20);                
    count2<<=1;
  }
  
  //CLK_SET();
  TXD_SET();
  udelay(20);
  CS_SET();
  udelay(100);

}


void init_lcd_para(void)
{
	init_lcd_io(); 	

#if 1
  WriteCommand(0x0029); // RESET
  WriteCommand(0x0025); // STANDBY
  WriteCommand(0x0240); // Enable Normally Black
  WriteCommand(0x0130); // Enable FRC/Dither
  WriteCommand(0x0e5f); // Enter Test mode(1)
  WriteCommand(0x0fa4); // Enter Test mode(2)
  WriteCommand(0x0d09); // Enable SDRRS, enlarge OE width.
  WriteCommand(0x1041); // Adopt 2 Line / 1 Dot
  udelay(100);					// wait 100ms
  WriteCommand(0x00ad); // DISPLAY ON
#else
  WriteCommand(0x0019); // RESET
  WriteCommand(0x0015); // STANDBY
  WriteCommand(0x0240); // Enable Normally Black
  WriteCommand(0x0130); // Enable FRC/Dither
  WriteCommand(0x0e5f); // Enter Test mode(1)
  WriteCommand(0x0fa4); // Enter Test mode(2)
  WriteCommand(0x0d09); // Enable SDRRS, enlarge OE width.
  WriteCommand(0x1041); // Adopt 2 Line / 1 Dot
  udelay(100);					// wait 100ms
  WriteCommand(0x009d); // DISPLAY ON
#endif
}

void resume_lcd_para(void)
{
#if 1
  WriteCommand(0x0029); // RESET
  WriteCommand(0x0025); // STANDBY
  WriteCommand(0x0240); // Enable Normally Black
  WriteCommand(0x0130); // Enable FRC/Dither
  WriteCommand(0x0e5f); // Enter Test mode(1)
  WriteCommand(0x0fa4); // Enter Test mode(2)
  WriteCommand(0x0d09); // Enable SDRRS, enlarge OE width.
  WriteCommand(0x1041); // Adopt 2 Line / 1 Dot
  udelay(100);					// wait 100ms
  WriteCommand(0x00ad); // DISPLAY ON
#else
  WriteCommand(0x0019); // RESET
  WriteCommand(0x0015); // STANDBY
  WriteCommand(0x0240); // Enable Normally Black
  WriteCommand(0x0130); // Enable FRC/Dither
  WriteCommand(0x0e5f); // Enter Test mode(1)
  WriteCommand(0x0fa4); // Enter Test mode(2)
  WriteCommand(0x0d09); // Enable SDRRS, enlarge OE width.
  WriteCommand(0x1041); // Adopt 2 Line / 1 Dot
  udelay(100);					// wait 100ms
  WriteCommand(0x009d); // DISPLAY ON
#endif
}


static void lcd_resume(struct work_struct *work);
static void lcd_late_resume(struct early_suspend *h);
static DECLARE_WORK(lcd_resume_work, lcd_resume);
static struct workqueue_struct *lcd_resume_wq;

static struct early_suspend lcd_early_suspend_desc = {
  .level = EARLY_SUSPEND_LEVEL_DISABLE_FB + 1, // before fb resume
  .resume = lcd_late_resume,
};

int init(void)
{

  if (gLcd_info)
  {
  	//printk("gLcd_info is not null\n");
    gLcd_info->io_init();
  }
  
	CS_CLR();
	mdelay(100);
	CS_SET();
	mdelay(100);
	CS_CLR();  

  init_lcd_para();

  if (gLcd_info)
    gLcd_info->io_deinit();

  lcd_resume_wq = create_singlethread_workqueue("hitachi_lcd");
  register_early_suspend(&lcd_early_suspend_desc);
  return 0;
}

int standby(u8 enable)	//***enable =1 means suspend, 0 means resume
{
	printk("zpp--traced:%s--%d--enable=%d\n",__FUNCTION__,__LINE__,enable);
	if(enable)
	{
		//WriteCommand(0x0029); // RESET
		WriteCommand(0x0025); // STANDBY
		//mdelay(100);
		suspend_lcd_io();
		if (gLcd_info)
		gLcd_info->io_deinit();
	}    
	else 
	{
		flush_workqueue(lcd_resume_wq);
	}
	return 0;
}


static void lcd_resume(struct work_struct *work)
{

	resume_lcd_io();

  if (gLcd_info)
    gLcd_info->io_init();
    
    
  CS_CLR();
  mdelay(100);
  CS_SET();
  mdelay(100);
  CS_CLR();  
    
  //init_lcd_para();
  resume_lcd_para();

  if (gLcd_info)
    gLcd_info->io_deinit();
}

static void lcd_late_resume(struct early_suspend *h)
{
  queue_work(lcd_resume_wq, &lcd_resume_work);
}

#endif

void set_lcd_info(struct rk29fb_screen *screen, struct rk29lcd_info *lcd_info )
{
    /* screen type & face */
    screen->type = OUT_TYPE;
    screen->face = OUT_FACE;

    /* Screen size */
    screen->x_res = H_VD;
    screen->y_res = V_VD;

    screen->width = LCD_WIDTH;
    screen->height = LCD_HEIGHT;

    /* Timing */
    screen->lcdc_aclk = LCDC_ACLK;
    screen->pixclock = OUT_CLK;
	screen->left_margin = H_BP;
	screen->right_margin = H_FP;
	screen->hsync_len = H_PW;
	screen->upper_margin = V_BP;
	screen->lower_margin = V_FP;
	screen->vsync_len = V_PW;

	/* Pin polarity */
	screen->pin_hsync = 0;
	screen->pin_vsync = 0;
	screen->pin_den = 0;
	screen->pin_dclk = DCLK_POL;

	/* Swap rule */
    screen->swap_rb = SWAP_RB;
    screen->swap_rg = 0;
    screen->swap_gb = 0;
    screen->swap_delta = 0;
    screen->swap_dumy = 0;

    /* Operation function*/
#if defined(CONFIG_LCD_SC_N71SI) 
  if (lcd_info)
    gLcd_info = lcd_info;

    screen->init = init;
    screen->standby = standby;
#else
    screen->init = NULL;
    screen->standby = NULL; 
#endif
}
